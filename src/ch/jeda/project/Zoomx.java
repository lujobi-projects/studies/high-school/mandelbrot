package ch.jeda.project;

import ch.jeda.*;
import ch.jeda.event.*;
import ch.jeda.ui.*;


public class Zoomx extends Program implements PointerListener {
    Window window;
    CoordinateSystem cs;
    double x_min = -3;
    double x_max = 1.2;
    double y_min = -1.2;
    double y_max = 1.2;
    double l_x;
    double l_y;
    int m = 100; // Anzahl Iterationen
    int i;
    int j;
    double v;
    int q=0;
    int red;
    int green;
    int blue;
    
    
    @Override
    public void run() {
        i = 1400; //Grösse des Feldes
        j = 800;
        v = i/j;
        window = new Window(i, j);
        
        window.setColor(Color.BLACK);
        window.fill();

        cs = new CoordinateSystem(i, j, x_min, x_max, y_min, y_max);

        draw();
        
        window.addEventListener(this);
    }
    
    public void draw() {
        
        double b = y_max; //y-Richtung
        int n = 0; //Zähler Iterationen
        int d = 20;
        
   
        
        while (b >= y_min){
           double a = x_min; //x-Richtung
           while (a <= x_max) {
                
                double x = 0.0; // Teil von z
                double y = 0.0; // Teil von z
                
                while (n < m){
                    double s; //Zwischenspeicher x
                            
                    s = x*x-y*y + a;
                    y = 2*x*y + b;
                    x = s;
                    n = n+1;
                    q=q+1;

                if (x*x+y*y > 4) {
                         
                        if (q==1){
                            red=255;
                        }
                        
                        else if (q==2){
                            green=255;
                        }
                        
                        else {
                            blue=255;
                        }

                        window.setColor(new Color(red, green, blue));
                        red=0;
                        green=0;
                        blue=0;
                        q=0;
                        
                        window.fillRectangle(cs.getJavaX(a), cs.getJavaY(b), 1, 1);
                        n = m;
                    }
                    if (q==3){
                        q=0;
                    }
                }

                a = a+cs.getStepX();
                n = 0;
           } 
           b = b-cs.getStepY();
       }
    }

    @Override
    public void onPointerDown(PointerEvent pe) {
       x_min = cs.getUserX((int)pe.getX());
       y_max = cs.getUserY((int)pe.getY());
    }

    @Override
    public void onPointerMoved(PointerEvent pe) {
        x_max = cs.getUserX((int)pe.getX());
        y_min = cs.getUserY((int)pe.getY());
        l_x = x_max - x_min;
        l_y = y_max - y_min;
        
        if (l_x * v > l_y){
            l_y = l_x/v;
        }
        else {
            l_x = l_y * v;
        }

    }

    @Override
    public void onPointerUp(PointerEvent pe) {
        window.setColor(Color.BLACK);
        window.fill();
        
        x_max = cs.getUserX((int)pe.getX());
        y_min = cs.getUserY((int)pe.getY()) ;
        l_x = x_max - x_min;
        l_y = y_max - y_min;
        
        if (l_x * v > l_y){
            l_y = l_x/v;
        }
        else {
            l_x = l_y * v;
        }
        
        
        cs = new CoordinateSystem(i, j, x_min, x_max, y_min, y_max);

        draw();
    }
}