package ch.jeda.project;

public class CoordinateSystem {
    private double x_min, x_max, x_diff;
    private double y_min, y_max, y_diff;

    private int canvas_width, canvas_height;


    
    public CoordinateSystem(int width, int height) {
    	this(width, height, -1, 1, -1, 1);
    }



    public CoordinateSystem(int width, int height, double x_min, double x_max,
                            double y_min, double y_max) {
    	this.canvas_width  = width;
    	this.canvas_height = height;
    	
        setCS(x_min, x_max, y_min, y_max);
    }



    public void setCS(double x_min, double x_max, double y_min, double y_max) {
        this.x_min = x_min;
        this.x_max = x_max;
        this.y_min = y_min;
        this.y_max = y_max;

        x_diff = Math.abs(x_max - x_min);
        y_diff = Math.abs(y_max - y_min);
    }



    public int getJavaX(double x_user) {
        double x_java = Math.abs(x_user - x_min) * canvas_width / x_diff;
        return (new Double(x_java)).intValue();
    }



    public int getJavaY(double y_user) {
        double y_java = Math.abs(y_user - y_max) * canvas_height / y_diff;

        return (new Double(y_java)).intValue();
    }



    public double getUserX(int x_java) {
        //return (x_java - applet_width/(x_diff/-x_min))/x_factor;
        double x_user = x_java * x_diff / canvas_width;
        return (x_min < x_max) ? x_min + x_user : x_min - x_user;
    }



    public double getUserY(int y_java) {
        //return (-y_java + applet_height/(y_diff/-y_min))/y_factor;
        double y_user = y_java * y_diff / canvas_height;
        return (y_min < y_max) ? y_max - y_user : y_max + y_user;
    }



    public double getStepX() {
        return x_diff / canvas_width;
    }



    public double getStepY() {
        return y_diff / canvas_height;
    }
}
