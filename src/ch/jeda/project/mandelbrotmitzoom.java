package ch.jeda.project;
 
import ch.jeda.*;
import ch.jeda.event.*;
import ch.jeda.ui.*;
 
/**
 *
 * @author Luzian
 */
public class mandelbrotmitzoom extends Program implements PointerDownListener, PointerUpListener {
 
    Window window;
    CoordinateSystem cs;
    double x_min = -3;
    double x_max = 1.2;
    double y_min = -1.2;
    double y_max = 1.2;
    double l_x;
    double l_y;
    int m = 10000; // Anzahl Iterationen
    int i;
    int j;
    double v;
    int q = 0;
    
    
    @Override
    public void run() {
        i = 1400; //Grösse des Feldes
        j = 800;
        v = i/j;
        window = new Window(i, j);
        cs = new CoordinateSystem(i, j, x_min, x_max, y_min, y_max);

        draw();
        
        window.addEventListener(this);
    }
    
    public void draw() {
        
        window.setColor(Color.BLACK);
        window.fill();
        
        double b = y_max; //y-Richtung
        int n = 0; //Zähler Iterationen
        int d = 20;
        
   
        
        while (b >= y_min){
           double a = x_min; //x-Richtung
           while (a <= x_max) {
                
                double x = 0.0; // Teil von z
                double y = 0.0; // Teil von z

                
                while (n < m){
                    double s; //Zwischenspeicher x
                            
                    s = x*x-y*y + a;
                    y = 2*x*y + b;
                    x = s;
                    n = n+1;
                    q = q+1;
                    
                    if (x*x+y*y > 4) {
                         
                        Color farbe = evaluateColor(q);
                        window.setColor(farbe);
                        q = 0;
                        
                        window.fillRectangle(cs.getJavaX(a), cs.getJavaY(b), 1, 1);
                        n = m;
                    }
                    if (q == 4){
                        q = 0;
                    }
                }

                a = a+cs.getStepX();
                n = 0;
           } 
           b = b-cs.getStepY();
       }
    }

    @Override
    public void onPointerDown(PointerEvent pe) {
        x_min = cs.getUserX((int)pe.getX());
        y_max = cs.getUserY((int)pe.getY());
    }
    
    @Override
    public void onPointerUp (PointerEvent pe){
        x_max = cs.getUserX((int)pe.getX());
        y_min = cs.getUserY((int)pe.getY());
        
        cs = new CoordinateSystem(i, j, x_min, x_max, y_min, y_max);
        draw();
    }
    
    
    
    public Color evaluateColor(int q) {
    int red=0;
    int green=0;
    int blue=0;
    
        if (q == 1){ //RED
            red = 255;
            green = 30;
        }

        else if (q == 2){ //YELLOW
            green = 255;
            red = 255;
            blue = 50;
        }

        else if (q == 3){ //BLUE
            blue = 255;
        }
        else if (q == 4){ //GREEN
            green = 200;
            blue = 50;
        }
       
        return new Color (red, green, blue); 
    }
}
  