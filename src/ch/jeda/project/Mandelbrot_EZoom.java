package ch.jeda.project;

import ch.jeda.*;
import ch.jeda.event.*;
import ch.jeda.ui.*;


public class Mandelbrot_EZoom extends Program implements PointerListener {
    Window window;
    CoordinateSystem cs;
    double x_min = -3;
    double x_max = 1.2;
    double y_min = -1.2;
    double y_max = 1.2;
    double l_x;
    double l_y;
    int m = 1000; // Anzahl Iterationen
    int i;
    int j;
    double v;
    
    public Color evaluateColor(int mein_n, int mein_m) {
        int green = 255* mein_n / mein_m;
        int red = 1;
        int blue = 255;
       
        return new Color (red, green, blue);   
    }
    
    @Override
    public void run() {
        i = 1400; //Grösse des Feldesq
        j = 800;
        v = i/j;
        window = new Window(i, j);
        cs = new CoordinateSystem(i, j, x_min, x_max, y_min, y_max);

        draw();
        
        window.addEventListener(this);
    }
    
    public void draw() {
        
        double b = y_max; //y-Richtung
        int n = 0; //Zähler Iterationen
        int d = 20;
        
   
        
        while (b >= y_min){
           double a = x_min; //x-Richtung
           while (a <= x_max) {
                
                double x = 0.0; // Teil von z
                double y = 0.0; // Teil von z

                
                while (n < m){
                    double s = 0.0; //Zwischenspeicher x
                            
                    s = x*x-y*y + a;
                    y = 2*x*y + b;
                    x = s;
                    n = n+1;
                    if (x*x+y*y > 4) {
                         
                        Color farbe = evaluateColor(n, m);
                        window.setColor(farbe);
                        window.fillRectangle(cs.getJavaX(a), cs.getJavaY(b), 1, 1);
                        n = m;

                    }
                }

                a = a+cs.getStepX();
                n = 0;
           } 
           b = b-cs.getStepY();
       }
    }

    @Override
    public void onPointerDown(PointerEvent pe) {
       x_min = cs.getUserX((int)pe.getX());
       y_max = cs.getUserY((int)pe.getY());
    }

    @Override
    public void onPointerMoved(PointerEvent pe) {
        x_max = cs.getUserX((int)pe.getX());
        y_min = cs.getUserY((int)pe.getY());
        l_x = x_max - x_min;
        l_y = y_max - y_min;
        
        if (l_x * v > l_y){
            l_y = l_x/v;
        }
        else {
            l_x = l_y * v;
        }

    }

    @Override
    public void onPointerUp(PointerEvent pe) {
        window.setColor(Color.WHITE);
        window.fill();
        
        x_max = cs.getUserX((int)pe.getX());
        y_min = cs.getUserY((int)pe.getY());
        l_x = x_max - x_min;
        l_y = y_max - y_min;
        
        if (l_x * v > l_y){
            l_y = l_x/v;
        }
        else {
            l_x = l_y * v;
        }
        
        
        cs = new CoordinateSystem(i, j, x_min, x_max, y_min, y_max);

        draw();
    }
}